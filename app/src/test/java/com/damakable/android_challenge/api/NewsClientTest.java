package com.damakable.android_challenge.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import retrofit2.Call;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the NewsClient.
 * <p>
 * Things to test:
 * <p>
 * searchForSomething
 * searchForNothing
 * searchForGarbage
 * searchForUnicode
 * searchForEmojis
 * searchFor'); DROP TABLE News;--
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsClientTest
{
    private final static String QUERY = "Something";

    @Mock private NewsService newsService;
    @Mock private SearchResultsCallback callback;
    @Mock private Call<SearchResults> call;

    @Test
    public void searchForSomething()
    {
        when(newsService.search(anyString(), anyInt())).thenReturn(call);

        NewsClient newsClient = new NewsClient(newsService, callback);
        SearchResultsCallback.SearchResultsConsumer consumer =
                mock(SearchResultsCallback.SearchResultsConsumer.class);
        newsClient.search(QUERY, 1, consumer);

        // Note: Not testing that the callback is called, only enqueued.
        verify(callback).setConsumer(consumer);
        verify(newsService).search(QUERY, 1);
        verify(call).enqueue(callback);
    }
}
