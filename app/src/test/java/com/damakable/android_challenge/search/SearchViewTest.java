package com.damakable.android_challenge.search;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.damakable.android_challenge.R;
import com.damakable.android_challenge.api.SearchResults;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the SearchView.
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchViewTest
{
    private static final String QUERY_STRING = "Something";

    @Mock private SearchPresenter presenter;
    @Mock private SearchResultsAdapter adapter;
    @Mock private View root;
    @Mock private EditText queryField;
    @Mock private RecyclerView articlesList;

    private SearchView createDefaultView()
    {
        Context context = mock(Context.class);
        SearchView view = new SearchView(context, presenter, adapter);
        LayoutInflater inflater = mock(LayoutInflater.class);
        ViewGroup viewGroup = mock(ViewGroup.class);
        LinearLayoutManager layoutManager = mock(LinearLayoutManager.class);
        DividerItemDecoration itemDecoration = mock(DividerItemDecoration.class);

        when(inflater.inflate(R.layout.activity_search_form, viewGroup, false))
                .thenReturn(root);
        when(root.findViewById(R.id.query_field)).thenReturn(queryField);
        when(root.findViewById(R.id.articles_list)).thenReturn(articlesList);

        view.onCreate(inflater, viewGroup, layoutManager, itemDecoration);

        verify(inflater).inflate(R.layout.activity_search_form, viewGroup, false);
        verify(articlesList).setAdapter(adapter);

        return view;
    }

    @Test
    public void testOnCreate()
    {
        createDefaultView();
    }

    @Test
    public void testOnSearchButtonClicked()
    {
        SearchView view = createDefaultView();

        Editable editable = mock(Editable.class);
        when(view.queryField.getText()).thenReturn(editable);
        when(editable.toString()).thenReturn(QUERY_STRING);

        view.onSearchButtonClicked();

        verify(presenter).performSearch(QUERY_STRING);
    }

    @Test
    public void testSetResults()
    {
        SearchView view = createDefaultView();
        SearchResults results = mock(SearchResults.class);

        view.setResults(results);

        verify(adapter).clear();
        verify(adapter).addAll(results);
    }

    @Test
    public void testAddResults()
    {
        SearchView view = createDefaultView();
        SearchResults results = mock(SearchResults.class);

        view.addResults(results);

        verify(adapter).addAll(results);
    }
}
