package com.damakable.android_challenge.search;

import com.damakable.android_challenge.api.NewsClient;
import com.damakable.android_challenge.api.SearchResults;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the SearchPresenter.
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterTest
{
    private static final String QUERY_STRING = "Something else";
    private static final String ERROR_STRING = "oh no";

    @Mock private NewsClient newsClient;
    @Mock private SearchView view;

    private SearchPresenter createDefaultPresenter()
    {
        SearchPresenter presenter = new SearchPresenter(newsClient);
        presenter.setView(view);
        return presenter;
    }

    @Test
    public void testPerformSearch()
    {
        SearchPresenter presenter = new SearchPresenter(newsClient);
        presenter.performSearch(QUERY_STRING);

        verify(newsClient).search(QUERY_STRING, 1, presenter);
    }

    @Test
    public void testOnResult()
    {
        SearchPresenter presenter = createDefaultPresenter();
        SearchResults results = mock(SearchResults.class);

        presenter.onResult(results);

        verify(view).setResults(results);
    }

    @Test
    public void testOnFailure()
    {
        SearchPresenter presenter = createDefaultPresenter();

        presenter.onFailure(ERROR_STRING);

        verify(view).showError(ERROR_STRING);
    }
}
