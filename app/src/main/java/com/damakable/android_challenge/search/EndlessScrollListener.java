package com.damakable.android_challenge.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * An OnScrollListener that notifies a presenter when its RecyclerView is near the bottom
 * of its list.
 *
 * Based on this example: https://gist.github.com/nesquena/d09dc68ff07e845cc622
 */
public class EndlessScrollListener extends RecyclerView.OnScrollListener
{
    private final static int VISIBLE_THRESHOLD = 5;

    private SearchPresenter presenter;
    private LinearLayoutManager layoutManager;

    private boolean loading;
    private int previousCount;

    EndlessScrollListener(LinearLayoutManager layoutManager, SearchPresenter presenter)
    {
        this.layoutManager = layoutManager;
        this.presenter = presenter;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy)
    {
        super.onScrolled(recyclerView, dx, dy);

        int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
        int itemCount = layoutManager.getItemCount();

        if (itemCount < previousCount)
        {
            previousCount = itemCount;
        }

        if (loading && (itemCount > previousCount))
        {
            loading = false;
            previousCount = itemCount;
        }

        if (!loading && (lastVisibleItemPosition + VISIBLE_THRESHOLD) > itemCount)
        {
            presenter.loadMore();
            loading = true;
        }
    }
}
