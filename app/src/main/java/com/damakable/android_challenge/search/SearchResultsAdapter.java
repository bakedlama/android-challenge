package com.damakable.android_challenge.search;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damakable.android_challenge.GlideApp;
import com.damakable.android_challenge.R;
import com.damakable.android_challenge.api.Article;
import com.damakable.android_challenge.api.SearchResults;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for SearchResults
 */
public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder>
{
    private List<Article> articles = new ArrayList<>();

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        CardView card;
        TextView headline;
        ImageView imageView;

        ViewHolder(View v)
        {
            super(v);
            card = v.findViewById(R.id.article_card);
            headline = v.findViewById(R.id.article_headline);
            imageView = v.findViewById(R.id.article_image);
        }
    }

    @Override
    public int getItemCount()
    {
        return articles.size();
    }

    void clear()
    {
        articles.clear();
    }

    void addAll(SearchResults results)
    {
        articles.addAll(results.getArticles());
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_article, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Article article = articles.get(position);

        holder.headline.setText(article.getTitle());

        final Context context = holder.card.getContext();
        String imageUrl = article.getUrlToImage();
        if (!imageUrl.isEmpty())
        {
            GlideApp.with(context).load(imageUrl).centerCrop().into(holder.imageView);
        }

        final String url = article.getUrl();
        holder.card.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });
    }
}
