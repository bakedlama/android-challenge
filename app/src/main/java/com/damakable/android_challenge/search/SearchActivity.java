package com.damakable.android_challenge.search;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.damakable.android_challenge.api.NewsClient;
import com.damakable.android_challenge.api.SearchResultsCallback;

/**
 * Activity for the SearchScreen.
 */
public class SearchActivity extends AppCompatActivity
{
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        SearchPresenter presenter = new SearchPresenter(
                new NewsClient(NewsClient.provideNewsService(), new SearchResultsCallback()));
        SearchResultsAdapter adapter = new SearchResultsAdapter();
        searchView = new SearchView(this, presenter, adapter);
        presenter.setView(searchView);
        searchView.onCreate(getLayoutInflater(), (ViewGroup) findViewById(android.R.id.content),
                new LinearLayoutManager(this),
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        setContentView(searchView.getRoot());
    }

    public void onSearchButtonClicked(View button)
    {
        searchView.onSearchButtonClicked();
    }
}
