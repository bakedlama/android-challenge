package com.damakable.android_challenge.search;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.damakable.android_challenge.R;
import com.damakable.android_challenge.api.SearchResults;

/**
 * View for the SearchActivity.
 */
class SearchView
{
    EditText queryField;

    private Context context;
    private SearchPresenter presenter;
    private View root;
    private SearchResultsAdapter adapter;

    SearchView(Context context, SearchPresenter presenter, SearchResultsAdapter adapter)
    {
        this.context = context;
        this.presenter = presenter;
        this.adapter = adapter;
    }

    void onCreate(LayoutInflater inflater, ViewGroup container,
                  LinearLayoutManager layoutManager, DividerItemDecoration itemDecoration)
    {
        root = inflater.inflate(R.layout.activity_search_form, container, false);
        queryField = root.findViewById(R.id.query_field);

        RecyclerView articlesList = root.findViewById(R.id.articles_list);
        articlesList.setLayoutManager(layoutManager);
        articlesList.addItemDecoration(itemDecoration);
        articlesList.setAdapter(adapter);
        articlesList.addOnScrollListener(new EndlessScrollListener(layoutManager, presenter));
    }

    void onSearchButtonClicked()
    {
        String query = queryField.getText().toString();
        presenter.performSearch(query);
    }

    void setResults(SearchResults results)
    {
        adapter.clear();
        adapter.addAll(results);
    }

    void addResults(SearchResults results)
    {
        adapter.addAll(results);
    }

    void showError(String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    View getRoot()
    {
        return root;
    }
}
