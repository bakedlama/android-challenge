package com.damakable.android_challenge.search;

import com.damakable.android_challenge.api.NewsClient;
import com.damakable.android_challenge.api.SearchResults;
import com.damakable.android_challenge.api.SearchResultsCallback;

/**
 * Presenter for the SearchView.
 */
class SearchPresenter implements SearchResultsCallback.SearchResultsConsumer
{
    private SearchView view;
    private NewsClient newsClient;
    private String query;
    private int currentPage = 1;

    SearchPresenter(NewsClient newsClient)
    {
        this.newsClient = newsClient;
    }

    void setView(SearchView view)
    {
        this.view = view;
    }

    void performSearch(String query)
    {
        this.query = query;
        currentPage = 1;
        newsClient.search(query, currentPage, this);
    }

    void loadMore()
    {
        currentPage++;
        newsClient.search(query, currentPage, this);
    }

    @Override
    public void onResult(SearchResults searchResults)
    {
        if (currentPage == 1)
        {
            view.setResults(searchResults);
        }
        else
        {
            view.addResults(searchResults);
        }
    }

    @Override
    public void onFailure(String message)
    {
        view.showError(message);
    }
}
