package com.damakable.android_challenge.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Fetches the news.
 */
public class NewsClient
{
    private static final String BASE_URL = "https://newsapi.org/v2/";

    private NewsService newsService;
    private SearchResultsCallback resultsCallback;

    /**
     * Pretty much a Dagger @Provides method.
     */
    public static NewsService provideNewsService()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(NewsService.class);
    }

    public NewsClient(NewsService newsService, SearchResultsCallback resultsCallback)
    {
        this.newsService = newsService;
        this.resultsCallback = resultsCallback;
    }

    public void search(String query, int page, SearchResultsCallback.SearchResultsConsumer consumer)
    {
        resultsCallback.setConsumer(consumer);
        newsService.search(query, page).enqueue(resultsCallback);
    }
}
