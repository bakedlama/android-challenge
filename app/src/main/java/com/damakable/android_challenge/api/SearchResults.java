package com.damakable.android_challenge.api;

import java.util.List;

/**
 * Model for search results.
 */
public class SearchResults
{
    String status;
    List<Article> articles;

    public SearchResults(String status, List<Article> articles)
    {
        this.status = status;
        this.articles = articles;
    }

    public List<Article> getArticles()
    {
        return articles;
    }
}
