package com.damakable.android_challenge.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Retrofit interface for the News API.
 *
 * Example from docs:
 * var url = 'https://newsapi.org/v2/everything?' +
 * 'q=Apple&' +
 * 'from=2017-12-06&' +
 * 'sortBy=popularity&' +
 * 'apiKey={API_KEY}';
 *
 * Only the query and apiKey are necessary.
 * We'll use the X-Api-Key header so that apiKey doesn't need to be passed with every call.
 */
public interface NewsService
{
    // TODO: Store this more securely!
    // Some things that would help:
    //  - Configure ProGuard
    //  - Encrypt and decode at runtime
    //  - Hide behind JNI
    //  - Retrieve the secret from a server
    String API_KEY_HEADER = "X-Api-Key: 7b0d032a41f64b10970ddd501c4e819c";

    @Headers(API_KEY_HEADER)
    @GET("everything?sortBy=popularity")
    Call<SearchResults> search(@Query("q") String query, @Query("page") int page);
}
