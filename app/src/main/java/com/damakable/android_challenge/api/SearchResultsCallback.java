package com.damakable.android_challenge.api;

import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This callback handles results from search requests.
 */
public class SearchResultsCallback implements Callback<SearchResults>
{
    public interface SearchResultsConsumer
    {
        void onResult(SearchResults searchResults);
        void onFailure(String reason);
    }

    private SearchResultsConsumer consumer;

    void setConsumer(SearchResultsConsumer consumer)
    {
        this.consumer = consumer;
    }

    @Override
    public void onResponse(@NonNull Call<SearchResults> call, @NonNull Response<SearchResults> response)
    {
        consumer.onResult(response.body());
    }

    @Override
    public void onFailure(@NonNull Call<SearchResults> call, @NonNull Throwable t)
    {
        consumer.onFailure(t.getMessage());
    }
}
