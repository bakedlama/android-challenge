package com.damakable.android_challenge.api;

import android.support.annotation.NonNull;

/**
 * Model for an article.
 */
public class Article
{
    class Source
    {
        String id;
        String name;
    }

    Source source;
    String author;
    String title;
    String description;
    String url;
    String urlToImage;
    String publishedAt;

    public Article() { }

    public Article(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public @NonNull String getUrl()
    {
        if (url == null)
            return "";

        return url;
    }

    public @NonNull String getUrlToImage()
    {
        if (urlToImage == null)
            return "";

        return urlToImage;
    }
}
