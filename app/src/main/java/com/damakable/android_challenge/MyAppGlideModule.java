package com.damakable.android_challenge;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Necessary to use Glide's generated API.
 */
@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
