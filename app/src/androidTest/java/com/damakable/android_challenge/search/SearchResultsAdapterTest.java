package com.damakable.android_challenge.search;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.damakable.android_challenge.R;
import com.damakable.android_challenge.api.Article;
import com.damakable.android_challenge.api.SearchResults;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

/**
 * Instrumented test for the SearchResultsAdapter
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class SearchResultsAdapterTest extends AndroidTestCase
{
    private final static String HELLO_WORLD = "Hello, world!";

    private SearchResultsAdapter adapter;
    private Context context;

    @Before
    public void setup() throws Exception
    {
        super.setUp();

        context = InstrumentationRegistry.getTargetContext();

        List<Article> articles = new ArrayList<>();
        articles.add(new Article(HELLO_WORLD));
        for (int i = 1; i <= 5; i++)
        {
            articles.add(new Article(String.valueOf(i)));
        }

        SearchResults results = new SearchResults("ok", articles);

        adapter = new SearchResultsAdapter();
        adapter.addAll(results);
    }

    @Test
    public void testGetItemCount()
    {
        Assert.assertEquals(6, adapter.getItemCount());
    }

    @Test
    public void testClear()
    {
        Assert.assertEquals(6, adapter.getItemCount());
        adapter.clear();
        Assert.assertEquals(0, adapter.getItemCount());
    }

    @Test
    public void testAddAll()
    {
        Assert.assertEquals(6, adapter.getItemCount());

        List<Article> articles = new ArrayList<>();
        for (int i = 0; i < 4; i++)
        {
            articles.add(new Article(String.valueOf(i)));
        }
        SearchResults results = new SearchResults("ok", articles);
        adapter.addAll(results);

        Assert.assertEquals(10, adapter.getItemCount());
    }

    @Test
    public void testOnCreateViewHolder()
    {
        ViewGroup parent = new LinearLayout(context);
        SearchResultsAdapter.ViewHolder holder = adapter.onCreateViewHolder(parent, 0);

        Assert.assertNotNull(holder.card);
        Assert.assertNotNull(holder.headline);
        Assert.assertNotNull(holder.imageView);
    }

    @Test
    public void onBindViewHolder()
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup parent = new LinearLayout(context);
        View v = inflater.inflate(R.layout.view_article, parent, false);
        SearchResultsAdapter.ViewHolder holder = new SearchResultsAdapter.ViewHolder(v);

        adapter.onBindViewHolder(holder, 0);

        Assert.assertEquals(HELLO_WORLD, holder.headline.getText().toString());

        for (int i = 1; i <= 5; i++)
        {
            adapter.onBindViewHolder(holder, i);
            Assert.assertEquals(String.valueOf(i), holder.headline.getText().toString());
        }
    }
}
