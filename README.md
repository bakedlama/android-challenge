# Technical Challenge Submission - Adam Blake

I started this Tuesday evening, so you're looking at three evenings worth of work.

Check out the wiki for some notes I made while planning and organizing my thoughts.

There's plenty of testing and polish I haven't had time for,
but hopefully this gives you some idea of my abilities!

# Android Developer Technical Challenge

This is a technical coding challenge for prospective android developer applicants

## Goal:

#### Develop a simple app that allows users to search and view articles from a news API site.

- [X] Fork this repo and make it public until we can review it.
    - Well, I didn't want to make this public until it was ready.  That's why we're on BitBucket!
- [X] Suggest using _Java_, _Kotlin_ or _C++_.
- [X] Use [this News API](https://newsapi.org/docs/endpoints/everything) for the app.
- [X] The app should have a search function.
- [X] Search results are displayed as a list.
- [X] When user taps on a news item. open it with a WebView or whatever you feel like using.
- [X] The app should work in both portrait and landscape orientations of the device.

### Optional:
- [ ] Data persistence.
- [X] Sleek UI elements and animation.
    - CardViews for that onclick ripple effect.
- [ ] List items should have dynamic height to fit the content.
- [ ] Display enough information on each list items.
- [X] User can scroll infinitely
- [ ] Be prepared for real world situation in terms of error handling, performance, etc.

### Evaluation:
- [X] Solution should compile. If there are necessary steps required to get it to compile, those should be covered in README.md.
- [X] Hopefully there won't be any crashes, bugs and compiler warnings.
    - Hopefully!
- [X] Unit tests
    - Some!
- [X] Ideally it should have clear, consistent and communicative coding style.
    - You tell me!
- [X] Code should conforms to SOLID principles.
    - I do my best!
- [X] Commit history is consistent, easy to follow and understand. If you want to show us how you handle pull requests, feel free to do so.
    - I didn't do any fancy issue tracking, branching, squashing, or the like, but I tried to commit regularly.